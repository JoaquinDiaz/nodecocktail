module.exports = function(app) {

  var Cocktail = require('../models/cocktails.js');

  //GET - Return all Cocktails in the DB
  findAllCocktails = function(req, res) {
  	Cocktail.find(function(err, Cocktails) {
  		if(!err) {
        console.log('GET /Cocktails')
  			res.send(Cocktails);
  		} else {
  			console.log('ERROR: ' + err);
  		}
  	});
  };

  //GET - Return a Cocktail with specified ID
  findById = function(req, res) {
  	Cocktail.findById(req.params.id, function(err, Cocktail) {
  		if(!err) {
        console.log('GET /Cocktail/' + req.params.id);
  			res.send(Cocktail);
  		} else {
  			console.log('ERROR: ' + err);
  		}
  	});
  };

  //POST - Insert a new Cocktail in the DB
  addCocktail = function(req, res) {
  	console.log('POST');
  	console.log(req.body);

  	var Cocktail = new Cocktail({
  		name:    		 req.body.name,
  		ingredients_id:  req.body.ingredients_id,
  	});

  	Cocktail.save(function(err) {
  		if(!err) {
  			console.log('Created');
  		} else {
  			console.log('ERROR: ' + err);
  		}
  	});

  	res.send(Cocktail);
  };

  //PUT - Update a register already exists
  updateCocktail = function(req, res) {
  	Cocktail.findById(req.params.id, function(err, Cocktail) {
  		Cocktail.name   = req.body.name;
  		Cocktail.ingredients_id    = req.body.ingredients_id;

  		Cocktail.save(function(err) {
  			if(!err) {
  				console.log('Updated');
  			} else {
  				console.log('ERROR: ' + err);
  			}
  		});
  	});
  }

  //DELETE - Delete a Cocktail with specified ID
  deleteCocktail = function(req, res) {
  	Cocktail.findById(req.params.id, function(err, Cocktail) {
  		Cocktail.remove(function(err) {
  			if(!err) {
  				console.log('Removed');
  			} else {
  				console.log('ERROR: ' + err);
  			}
  		})
  	});
  }

  //Link routes and functions
  app.get('/cocktails', findAllCocktails);
  app.get('/cocktail/:id', findById);
  app.post('/cocktail', addCocktail);
  app.put('/cocktail/:id', updateCocktail);
  app.delete('/cocktail/:id', deleteCocktail);

}