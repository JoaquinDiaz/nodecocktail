var express     = require("express"),
    app         = express(),
    http        = require("http"),
    server      = http.createServer(app),
    mongoose    = require('mongoose'),
    path        = require("path"),
    swig        = require('swig'),
    seed        = require('./models/seeder.js'),
    ingredients = require('./models/ingredients.js'),
    cocktails   = require('./models/cocktails.js');


seed.init();

app.configure(function () {
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
});

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.set('view cache', false);
swig.setDefaults({ cache: false });


mongoose.connect('mongodb://localhost/coktails', function(err, res) {
	if(err) {
		console.log('ERROR: connecting to Database. ' + err);
	} else {
		console.log('Connected to Database');
	}
});



app.get('/', function (req, res) {
    res.render('cocktails', {
            ingredients: {
                foo : {
                    name: 'foo',
                    price :200
                },
                foo1 : {
                    name: 'foo1',
                    price :201
                }
            }
    }); //Inject from Models
});

server.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});