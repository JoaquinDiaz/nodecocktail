var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var cocktail = new Schema({
	name: 		    { type: String },
	ingredients_id: { type: Array }
});


module.exports = mongoose.model('Cocktails', cocktail);