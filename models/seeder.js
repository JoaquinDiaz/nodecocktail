module.exports = {

    init: function () {

        var Ingredient = require('../models/ingredients.js');
        var Cocktail  = require('../models/cocktails.js');

        var lime     = new Ingredient({name: 'lime', price: 0.5});
        var tequilla = new Ingredient({name: 'tequilla', price: 0.5});

        var FooCocktail = new Cocktail({
            name:'FooCocktail',
            ingredients_id: [ lime._id,tequilla._id ]
        });
    }

}