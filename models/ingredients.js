var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ingredient = new Schema({
    name: 		 { type: String },
    price: 		 { type: Number }
});

module.exports = mongoose.model('Ingredients', ingredient);